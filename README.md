# poc_kubernetes
Working code can showcase to Customers

---

MediaWiki is a collaboration and documentation 
platform brought to you by a vibrant community

We will deploy this in Kubernetes cluster manually 
using the kubectl and optimized way using helm chat 

---

### Manual deployment Kubernetes.
>check kubernetes folder
### Automation deployment Helm.
>check helm folder - **NOTE: Not Started**

## **Before you begin:**

---
1, You need to have a Kubernetes cluster, and the kubectl 
   command-line tool must be configured to communicate with your cluster

2, Once you have the cluster ready. Follow the below Steps to 
   deploy the Mediawiki app and Storage.

---

### Steps to Follow.

1, To access the kubernetes cluster add the below command in your terminal.

```
gcloud config set project version1
```
2, Check the pods.
```
kubectl get pods
``` 
3, Check the deployments
```
kubectl get deployments
```
4, Deploy the Mediawiki app.
```
kubectl apply -f mediawiki_deployment.yml
```
5, Deploy the Presistant Volume.
```
kubectl apply -f mediawiki_db_volume.yml
```
6, Deploy the Secrets for DB.
```
kubectl apply -f mediawiki_db_secrets.yml
```
7, Deploy the database.
```
kubectl apply -f mediawiki_database.yml
```
8, Deploy the App service.
```
kubectl apply -f  mediawiki_service.yml
```
9, Deploy the DB service.
```
kubectl apply -f mediawiki_db_service.yml
```

## **When you want to upgrade the version**
---
1, We need to update the service.yml label to version 2.

2, Create the ConfigMap. Find the command below to create ConfigMap.

3, deploy the version 2 app.

4, deploy the service again.

---

10, Deploy the App v2 
```
kubectl apply -f mediawiki_deployment_v2.yml
```
11, Deploy the App service.
```
kubectl apply -f  mediawiki_service.yml
```

### Secrets need to be base64 encoded. 
Use the same in secrets.yml file.
----
Username:
```
####echo -n 'wikiuser' | base64
```
Password:
```
####echo -n 'dummy' | base64
```
----

### Create the config file

```
kubectl create configmap local-config-v1 --from-file=LocalSettings.php
```

## Note: For more details about the App. Please refer the below link. 
```
https://www.mediawiki.org/wiki/MediaWiki
```
## Note: For more details about the Kubernetes. Please refer the below link.
```
https://kubernetes.io/docs/home/
```
### NOTE: We should use the git-crypt since its a demo version and my system not supporting git-crypt hence not encrypted the db_secret yml file. 